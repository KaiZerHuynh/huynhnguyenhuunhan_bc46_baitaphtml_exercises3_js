const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
});
function loaiKH()
{
    var loaiKH = document.getElementById("khachHang").value;
    if(loaiKH == "nhaDan"){
        document.getElementById("soKetNoiDay").style = "display:none";
    }
    else{
        document.getElementById("soKetNoiDay").style = "display:flex";
    }
}

function phiCoban(khachHang){
    var soDay = document.getElementById("soDay").value*1;
    if(khachHang == "nhaDan")
    {
        return 20.5;
    }
    else if(khachHang == "doanhNghiep")
    {
        if(soDay <= 10)
        {
            return 75;
        }
        else{
            return 75 + (soDay - 10)* 5;
        }
    }
}

function phiHoadon(khachHang)
{
    if(khachHang == "nhaDan")
    {
        return 4.5;
    }
    else if(khachHang == "doanhNghiep")
    {
        return 15;
    }
}

function tinhTien()
{
    var maSo = document.getElementById("maSo").value*1;
    var khachHang = document.getElementById("khachHang").value;
    var tong = 0;
    var connect = document.getElementById("soKenh").value*1;
    if(khachHang == "nhaDan")
    {
        tong = phiCoban(khachHang) + phiHoadon(khachHang) + connect * 7.5;
    }
    else if(khachHang == "doanhNghiep")
    {
        tong = phiCoban(khachHang) + phiHoadon(khachHang) + connect * 50 ;
    }
    document.getElementById("result").innerText=`Mã số khách hàng là ${maSo};Tổng tiền cáp bạn là: $${formatter.format(tong)}`;
}