function tinhTien(){
    var tong = 0;
    var soKW = document.getElementById("soKW").value*1;
    var hoTen = document.getElementById("hoten").value;
    if(soKW <= 50)
    {
        tong = soKW*500;
    }
    else if(soKW > 50 && soKW <= 100)
    {
        tong = 50*500 + (soKW-50)*650;
    }
    else if(soKW > 100 && soKW <= 200)
    {
        tong = 50*500 + 50 * 650 + (soKW-100)*850;
    }
    else if(soKW > 200 && soKW < 350)
    {
        tong = 50*500 + 50 * 650 + 100*850 + 1100*(soKW-200);
    }
    else{
        tong = 50*500 + 50 * 650 + 100*850 + 150*1100 + 1400*(soKW-350);
    }
    document.getElementById("result").innerText=`Họ tên:${hoTen}; Số tiền bạn phải trả là ${tong}`;
}