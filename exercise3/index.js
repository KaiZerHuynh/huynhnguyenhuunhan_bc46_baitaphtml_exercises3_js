function tinhTien(){
    var tong = 0;
    var thuNhap = document.getElementById("thuNhap").value*1;
    var nguoi = document.getElementById("people").value*1;
    var hoTen = document.getElementById("hoten").value;
    if(thuNhap <= 60e+6)
    {
        tong = (thuNhap - 4e+6 - nguoi*(1.6e+6))*0.05;
    }
    else if(thuNhap > 60e+6 && thuNhap <= 120e+6)
    {
        tong = (thuNhap - 4e+6 - nguoi*(1.6e+6))*0.1;
    }
    else if(thuNhap > 120e+6 && thuNhap <= 210e+6)
    {
        tong = (thuNhap- 4e+6 - nguoi*(1.6e+6) )*0.15;
    }
    else if(thuNhap > 210e+6 && thuNhap <= 384e+6)
    {
        tong = 0.2*(thuNhap- 4e+6 - nguoi*(1.6e+6));
    }
    else if(thuNhap > 384e+6 && thuNhap <= 624e+6)
    {
        tong = 0.25*(thuNhap -  4e+6 - nguoi*(1.6e+6));
    }
    else if(thuNhap > 624e+6 && thuNhap <= 960e+6)
    {
        tong = 0.3*(thuNhap -  4e+6 - nguoi*(1.6e+6));
    }
    else{
        tong = 0.35*(thuNhap- 4e+6 - nguoi*(1.6e+6));
    }
    document.getElementById("result").innerText=`Họ tên:${hoTen}; Số tiền thuế cá nhân là ${new Intl.NumberFormat('vn-VN').format(tong)} VND`;
}